//
//  main.m
//  ParallaxBackground
//
//  Created by dtann80 on 7/12/13.
//  Copyright dtann 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    @autoreleasepool {
        
    
   // NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
  //  [pool release];
    return retVal;
    }
}
