//
//  IntroLayer.h
//  ParallaxBackground
//
//  Created by dtann80 on 7/12/13.
//  Copyright dtann 2013. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface IntroLayer : CCLayer
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
