//
//  EndlessParallaxNode.h
//  ParallaxBackground
//
//  Created by dtann80 on 7/12/13.
//  Copyright (c) 2013 dtann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCSprite.h"
#import "CCNode.h"
#import "CCArray.h"
#import "CGPointExtension.h"
#import "CCDirector.h"


@interface InfiniteParallaxNode : CCNode
{

	CGSize screenSize;
    
    ccArray	*_parallaxArray; //for holding InfiniteParrallaxObjects
    
    CGPoint _deltaPosition;
    
}


-(void)moveBy:(CGPoint)deltaPosition;
-(void) addChild: (CCSprite*)node z:(NSInteger)z parallaxRatio:(CGPoint)c;

@end
