//
//  InfiniteParallaxBatchNode.h
//  ParallaxBackground
//
//  Created by dtann80 on 7/13/13.
//  Copyright (c) 2013 dtann. All rights reserved.
//

#import "CCSpriteBatchNode.h"
#import "CCSprite.h"
#import "CCArray.h"
#import "CGPointExtension.h"
#import "CCDirector.h"

@interface InfiniteParallaxBatchNode : CCSpriteBatchNode
{
    
	CGSize screenSize;
    
    ccArray	*_parallaxArray; //for holding InfiniteParrallaxObjects
    
    CGPoint _deltaPosition;
    
}


-(void)moveBy:(CGPoint)deltaPosition;
-(void) addChild: (CCSprite*)node z:(NSInteger)z parallaxRatio:(CGPoint)c;


@end
