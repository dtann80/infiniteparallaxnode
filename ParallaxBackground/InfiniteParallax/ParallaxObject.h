//
//  ParallaxObject.h
//  ParallaxBackground
//
//  Created by dtann80 on 7/13/13.
//  Copyright (c) 2013 dtann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCSprite.h"

@interface ParallaxObject : NSObject

@property (nonatomic,readwrite) CGPoint ratio;
@property (nonatomic,readwrite) CGPoint offset;  //not used
@property (nonatomic,weak) CCSprite *child;

@end
