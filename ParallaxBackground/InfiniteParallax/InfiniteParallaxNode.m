//
//  EndlessParallaxNode.m
//  ParallaxBackground
//
//  Created by dtann80 on 7/12/13.
//  Copyright (c) 2013 dtann. All rights reserved.
//

#import "InfiniteParallaxNode.h"
#import "ParallaxObject.h"

@implementation InfiniteParallaxNode



-(id) init
{
	if ((self = [super init]))
	{
        
        _parallaxArray = ccArrayNew(5);
        
		// The screensize never changes during gameplay, so we can cache it in a member variable.
		screenSize = [[CCDirector sharedDirector] winSize];
		
    }
	
	return self;
}

-(void)moveBy:(CGPoint)deltaPosition
{
    _deltaPosition = ccpAdd(_deltaPosition, deltaPosition);
}

-(void) addChild:(CCNode*)child z:(NSInteger)z tag:(NSInteger)tag
{
	NSAssert(NO,@"InfiniteParrallaxNode: use addChild:z:parallaxRatio:positionOffset instead");
}

-(void) addChild: (CCSprite*) child z:(NSInteger)z parallaxRatio:(CGPoint)ratio
{
	NSAssert( child != nil, @"Argument must be non-nil");
    CCSprite *child2 = [[CCSprite alloc] initWithSpriteFrame:child.displayFrame];
    child.flipX = YES;
        
	child.anchorPoint = CGPointMake(0, 0.5f);
    child2.anchorPoint = CGPointMake(0, 0.5f);
    
    child.position = CGPointMake(0, screenSize.height / 2);
    child2.position = CGPointMake(screenSize.width - 1, screenSize.height / 2);
    
    ParallaxObject *obj = [[ParallaxObject alloc] init];
    obj.ratio = ratio;
	obj.child = child;
	ccArrayAppendObjectWithResize(_parallaxArray, obj);
    
    ParallaxObject *obj2 = [[ParallaxObject alloc] init];
    obj2.ratio = ratio;
	obj2.child = child2;
	ccArrayAppendObjectWithResize(_parallaxArray, obj2);
    
	[super addChild: child z:z tag:child.tag];
    [super addChild: child2 z:z tag:child2.tag];
}


-(void) removeChild:(CCNode*)node cleanup:(BOOL)cleanup
{
	for( unsigned int i=0;i < _parallaxArray->num;i++) {
		ParallaxObject *point = _parallaxArray->arr[i];
		if( [point.child isEqual:node] ) {
			ccArrayRemoveObjectAtIndex(_parallaxArray, i);
			break;
		}
	}
	[super removeChild:node cleanup:cleanup];
}

-(void) removeAllChildrenWithCleanup:(BOOL)cleanup
{
	ccArrayRemoveAllObjects(_parallaxArray);
	[super removeAllChildrenWithCleanup:cleanup];
}


-(void)visit
{
        
    for(unsigned int i=0; i < _parallaxArray->num; i++ ) {
        
        ParallaxObject *object = _parallaxArray->arr[i];
        float x = _deltaPosition.x*object.ratio.x;
        float y = _deltaPosition.y*object.ratio.y;
        
        
       CGPoint pos = ccpAdd(ccp(x, y), object.child.position);
        
        // Reposition stripes when they're out of bounds
		if (pos.x < -screenSize.width)
		{
			pos.x += (screenSize.width * 2) - 2;
		}
		
        object.child.position = pos;
    }
    
    
    _deltaPosition = CGPointZero;
    
    [super visit];
}

@end
